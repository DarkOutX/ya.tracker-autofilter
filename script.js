async function waitPageLoading() {
	let header = document.querySelector('header.agile-layout-header');

	while (!header) {
		await new Promise((resolve) => {
			setTimeout(resolve, 50);
		});
		
		header = document.querySelector('header.agile-layout-header');
	}
}

const FILTER_BTN_SELECTOR = 'button.agile-quick-filters-item';
const FILTER_BTN_SELECTED_MARKER = 'yc-button_view_normal';
const FILTER_SEPARATOR = '&';

function _isSkipFilterBtn(innerText) {
	return innerText !== 'Планирование' && !innerText.startsWith('Dev:');
}

function _getEnabledFilters() {
	const filterString = location.hash.replace('#/', '');
	const isNoFilters = filterString === '';

	if (isNoFilters) {
		return new Set([]);
	}

	return new Set(filterString.split(FILTER_SEPARATOR).map(str => str.toUpperCase()));
}

function _setFiltersURL(enabledFilters) {
	const locHash = `#/${[ ...enabledFilters ].join(FILTER_SEPARATOR)}`;
	const newURL = `${location.origin}${location.pathname}${locHash}`;

	window.history.replaceState( {} , document.title, newURL );
}

async function autoClick() {
	await waitPageLoading();

	const btns_all = [ ...document.querySelectorAll(FILTER_BTN_SELECTOR) ];
	const btns_custom = btns_all.filter(btn => _isSkipFilterBtn(btn.innerText));
	const enabledFilters = _getEnabledFilters();

	// Actualizing filters
	for (const btn of btns_custom) {
		const isSelected = btn.classList.contains(FILTER_BTN_SELECTED_MARKER);
		const filterName = btn.innerText.toUpperCase();
		const isInFilter = enabledFilters.has(filterName);

		const isNeedClick = (isSelected && !isInFilter) || (!isSelected && isInFilter);

		if (isNeedClick) {
			setTimeout(() => btn.click(), 10);
		}
	}

	// Handling clicks to update URL
	btns_custom.forEach(btn => {
		const filterName = btn.innerText.toUpperCase();

		btn.onclick = () => {
			const isInFilter = enabledFilters.has(filterName);
			const isSelected = btn.classList.contains(FILTER_BTN_SELECTED_MARKER);
			
			if (isSelected && isInFilter) {
				enabledFilters.delete(filterName);
			}
			else if (!isSelected && !isInFilter) {
				enabledFilters.add(filterName);
			}

			_setFiltersURL(enabledFilters);
		}
	});
}

window.onload = autoClick;
window.addEventListener('popstate', autoClick);